require 'test_helper'
require 'level1/level1'
require 'level2/level2'
require 'level3/level3'

class JsonDataControllerTest < ActionDispatch::IntegrationTest
  test "Is output equal to expected one (LEVEL 1) ?" do
  	res = LevelOne.new
	actual = res.getdata("test/models/test1/")
	expected = File.read(Rails.root + 'test/models/test1/expectedData.json')
  	assert_equal( expected, actual )
  end

  test "Is output equal to expected one (LEVEL 2) ?" do
  	res = LevelTwo.new
	actual = res.getdata("test/models/test2/")
	expected = File.read(Rails.root + 'test/models/test2/expectedData.json')
  	assert_equal( expected, actual )
  end

  test "Is output equal to expected one (LEVEL 3) ?" do
  	res = LevelThree.new
	actual = res.getdata("test/models/test3/")
	expected = File.read(Rails.root + 'test/models/test3/expectedData.json')
  	assert_equal( expected, actual )
  end

end
