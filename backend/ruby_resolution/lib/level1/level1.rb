require 'json'
class LevelOne
	def getdata(dataPath)
		path = dataPath.nil? ? 'lib/level1/' : dataPath
		file = File.read(Rails.root + path + 'data.json')
		data_hash = JSON.parse(file);

		output = Array.new

		data_hash['carts'].each do |cart|
			totalPerCart = 0
			cart['items'].each do |cartItem|
				data_hash['articles'].each do |article|
					if cartItem['article_id'] == article['id']
		        totalPerCart += (article['price'] * cartItem['quantity'])
		      end
				end
			end
			insert_this_cart = { :id => cart["id"], :total => totalPerCart }
			output.push(insert_this_cart)
		end

		carts = {:carts => output}.to_json
		if dataPath.nil?
			File.write(Rails.root + 'lib/level1/output_result_test_1.json', carts)
		end

		return carts;
	end
end