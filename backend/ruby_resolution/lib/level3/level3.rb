require 'json'
class LevelThree
	def getdata(dataPath)
		def calculatesTotalPrice(articleId, quantity, articles)
			totalprice = 0;
			articles.each do |article|
				if article['id'] == articleId
					totalprice = article['price'] * quantity;
				end
			end
			return totalprice
		end

		def calculateDiscounts(articleId, quantity, totalprice, discounts)
			discounts.each do |discount|
				if discount['article_id'] == articleId
					if discount['type'] == "amount"
						totalprice -= discount['value'] * quantity
					else
						totalprice -= (totalprice * discount['value']).to_f / 100
					end
				end
			end
			return totalprice.floor
		end

		def calculateTotalValue(dataPath)
			path = dataPath.nil? ? 'lib/level3/' : dataPath
			file = File.read(Rails.root + path + 'data.json')
			data_hash = JSON.parse(file);
			output = Array.new
			data_hash['carts'].each do |cart|
				totalPerCart = 0
				totalPerCartNoDiscount = 0
				cart['items'].each do |cartItem|
					itemsPriceWithoutDiscount = calculatesTotalPrice(cartItem['article_id'], cartItem['quantity'], data_hash['articles'])
					itemsFinalPrice = calculateDiscounts(cartItem['article_id'], cartItem['quantity'], itemsPriceWithoutDiscount, data_hash['discounts'])
					totalPerCart += itemsFinalPrice
					totalPerCartNoDiscount += itemsPriceWithoutDiscount
				end
				data_hash['delivery_fees'].each do |fee|
					min = (fee['eligible_transaction_volume'])['min_price']
					max = (fee['eligible_transaction_volume'])['max_price']
					max == nil ? max = 0 : max;
					if (min <= totalPerCartNoDiscount && max >= totalPerCartNoDiscount)
						totalPerCart += fee['price']
					end
				end
				insert_this_cart = { :id => cart["id"], :total => totalPerCart }
				output.push(insert_this_cart)
			end
			return output
		end

		carts = {:carts => calculateTotalValue(dataPath)}.to_json

		if dataPath.nil?
			File.write(Rails.root + 'lib/level3/output_result_test_3.json', carts)
		end

		return carts
	end
end