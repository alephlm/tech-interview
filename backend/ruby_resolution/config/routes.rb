Rails.application.routes.draw do
  get 'welcome/index'
  get 'getdataone', controller: 'json_data', action: :level1
  get 'getdatatwo', controller: 'json_data', action: :level2
  get 'getdatathree', controller: 'json_data', action: :level3
  root 'json_data#level1'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
