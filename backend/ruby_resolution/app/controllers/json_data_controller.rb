require 'level1/level1'
require 'level2/level2'
require 'level3/level3'

class JsonDataController < ApplicationController
	def level1
		res = LevelOne.new
		render :json => res.getdata(nil)
    end
	def level2
		res = LevelTwo.new
		render :json => res.getdata(nil)
    end
	def level3
		res = LevelThree.new
		render :json => res.getdata(nil)
    end
end