# General considerations
- This project usese a Rails framework for build an API sending json data to browser and creating an output file in filesystem.
- All levels challenges folders are at /lib/level[1-3]
- Source data has to be provided in each folder as 'data.json'

# Dependencies
To get dependencies gems, you need to run bundle
```sh
$ bundle install
```

# Running
To run the main program you only need to start a server at this folder
```sh
$ rails s
```
When started the server root path will show the solution for the first level.
here are the url's for levels results:

* http://localhost:3000/getdataone for level one.
* http://localhost:3000/getdatatwo for level two.
* http://localhost:3000/getdatathree for level three.


# Tests
A test class is at 'test/models/json_data_test.rb'
This class validates if the code generates an accurate output for a fixed input.
There are three test folders ea one has 'data.json' as input and 'expectedData.json' as expected output.
The generated output is compared to 'expectedData.json'.

For test execution you should run:
```sh
$ rails test test/models/json_data_test.rb
```

# SYS versions
Tested against

- Ruby version 2.3.1
- Rails version 5.0.1 