var fs = require('fs');

/***
This is the main function that gets each item from each cart and makes
a join using a map loop with articles to calculate the total amount
***/
var calculatesTotalPerCart = function (data, callback) {
  var output = [];
  try {
    data.carts.map(function (cart) {
      var totalPerCart = 0;
      var totalPerCartNoDiscount = 0;
      
      cart.items.map(function (cartItem) {
        itemsPriceWithoutDiscount = calculatesTotalPrice(cartItem.article_id, cartItem.quantity, data.articles)
        var itemsFinalPrice = calculateDiscounts(cartItem.article_id, cartItem.quantity, itemsPriceWithoutDiscount, data.discounts)
        totalPerCart += itemsFinalPrice;
        totalPerCartNoDiscount += itemsPriceWithoutDiscount;
      });
      //cauculates delivery fee based on original price
      data.delivery_fees.map(function (fee) {
        if (fee.eligible_transaction_volume.min_price <= totalPerCartNoDiscount &&
          fee.eligible_transaction_volume.max_price >= totalPerCartNoDiscount) {
          totalPerCart += fee.price;
        }
      });

      output.push({ id: cart.id, total: totalPerCart });
    });
  } catch (err) {
  }
  callback({ carts: output });
}

function calculatesTotalPrice(articleId, quantity, articles) {
  var totalprice = 0;
  articles.map(function (article) {
    if (article.id == articleId) {
      totalprice = article.price * quantity;
    }
  });
  return totalprice;
}

function calculateDiscounts(articleId, quantity, totalprice, discounts) {
  discounts.map(function (discount) {
    if (discount.article_id == articleId) {
      if (discount.type == "amount") {
        totalprice -= discount.value * quantity;
      } else {
        totalprice -= totalprice * discount.value / 100;
      }
    }
  });
  return Math.trunc(totalprice);
}

/***
Here we declare a function that calls or mais function and generates an output json file.
***/
var getTotalByCart = function (data, cb) {
  calculatesTotalPerCart(data, function (res) {
    if (cb != undefined) {
      return cb(res);
    } else {
      fs.writeFile("./output_result_test_3.json", JSON.stringify(res), function (err) {
        if (err) { return console.log(err); }
      });
    }
  });
}

exports.getTotalByCart = getTotalByCart;