var fs = require('fs');

/***
This is the main function that gets each item from each cart and makes
a join using a map loop with articles to calculate the total amount
***/
var calculatesTotalPerCart = function (data, callback) {
  var output = [];
  try {
    data.carts.map(function (cart) {
      var totalPerCart = 0;
      cart.items.map(function (cartItem) {
        data.articles.map(function (article) {
          if (cartItem.article_id == article.id) {
            totalPerCart += (article.price * cartItem.quantity);
          };
        });
      });
      output.push({ id: cart.id, total: totalPerCart });
    });
  } catch (err) {
    console.log("Something went wrong:", err.message);
  }
  callback({ carts: output });
}

/***
Here we declare a function that calls or mais function and generates an output json file.
***/
var getTotalByCart = function (data, cb) {
  calculatesTotalPerCart(data, function (res) {
    if (cb != undefined) {
      return cb(res);
    } else {
      fs.writeFile("./output_result_test_1.json", JSON.stringify(res), function (err) {
        if (err) { return console.log(err); }
      });
    }
  });
}

exports.getTotalByCart = getTotalByCart;