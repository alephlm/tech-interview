exports.inputSchema = {
  "type": "object",
  "properties": {
    "articles": {
      "type": "array",
      minItems: 1,
      "items": {
        "type": "object",
        "properties": {
          "id": {
            "type": "integer"
          },
          "name": {
            "type": "string"
          },
          "price": {
            "type": "integer"
          }
        },
        "required": [
          "id",
          "name",
          "price"
        ]
      }
    },
    "carts": {
      "type": "array",
      minItems: 1,
      "items": {
        "type": "object",
        "properties": {
          "id": {
            "type": "integer"
          },
          "items": {
            "type": "array",
            "items": {}
          }
        },
        "required": [
          "id",
          "items"
        ]
      }
    }
  },
  "required": [
    "articles",
    "carts"
  ]
};