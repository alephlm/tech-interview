var checkout = require("../app/api");
var data = require('../data.json');
var fs = require('fs');
var chai = require('chai');
var inputSchema = require('./inputSchema').inputSchema;
chai.use(require('chai-fs'));
chai.use(require('chai-json-schema'));

checkout.getTotalByCart(data);

describe('Testing JSON input file for schema compatibility.', function() {
    it('data.json exist and is a valid JSON file?', function() {
        chai.expect("./data.json").to.be.a.file("JSON data providaded is not compatible.")
            .with.json.using.schema(inputSchema);
    });
});

describe('Testing default json file results.', function() {
    it('Generated file from testData.json is the same as testOutput.json?', function() {
        var dataTest = require('./testData.json');
        checkout.getTotalByCart(dataTest, function(res) {
            var expectedResult = require('./testOutput.json');
            chai.expect(res).to.eql(expectedResult);
        });
    });
});
