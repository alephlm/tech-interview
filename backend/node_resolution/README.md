# General considerations
- For read and manipulate json data provided I chose to use Node.js.
[ What's better then JavaScript to work with JavaScript Object Notation? :) ]
- For code tests I used mocha.js with chai.js.
- Since it was not required DB connection or expose it to web api I chose to generate the response in the same folder as data.json.
- When executed json files will be generated according this pattern "output_result_test_[1-3].json".

# Requirements
- This files requires [Node.js](https://nodejs.org/) to run.

# Running
Each folder is a stand alone project.
Open the terminal or cmd and navigate to folder [level1, level2, level3].
```sh
$ cd ./tech-interview/backend/level[1-3]
```
Inside each folder you need to run the command:
```sh
$ npm install
```
It will install all the dependencies used in tests.
When the download is compleat you can run
```sh
$ npm start
```
For run the program and generate a json output
And
```sh
$ npm test
```
For run the test cases against the code.

# What is tested
The test case validates if the schema of the input file (data.json) is valid.
It also validates if the code generates a good output for a fixed input. The output is compared to an expected one.