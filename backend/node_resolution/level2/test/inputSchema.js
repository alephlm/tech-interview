exports.inputSchema = {
  "type": "object",
  "properties": {
    "articles": {
      "type": "array",
      minItems: 1,
      "items": {
        "type": "object",
        "properties": {
          "id": {
            "type": "integer"
          },
          "name": {
            "type": "string"
          },
          "price": {
            "type": "integer"
          }
        },
        "required": [
          "id",
          "name",
          "price"
        ]
      }
    },
    "carts": {
      "type": "array",
      minItems: 1,
      "items": {
        "type": "object",
        "properties": {
          "id": {
            "type": "integer"
          },
          "items": {
            "type": "array",
            "items": {}
          }
        },
        "required": [
          "id",
          "items"
        ]
      }
    },
    "delivery_fees": {
      "type": "array",
      minItems: 1,
      "items": {
        "type": "object",
        "properties": {
          "eligible_transaction_volume": {
            "type": "object",
            "properties": {
              "min_price": {
                "type": ["integer", "null"]
              },
              "max_price": {
                "type": ["integer", "null"]
              }
            },
            "required": [
              "min_price",
              "max_price"
            ]
          },
          "price": {
            "type": "integer"
          }
        },
        "required": [
          "eligible_transaction_volume",
          "price"
        ]
      }
    }
  },
  "required": [
    "articles",
    "carts",
    "delivery_fees"
  ]
}